﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Cocinitas.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "usuarios",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    administrador = table.Column<bool>(nullable: false),
                    email = table.Column<string>(nullable: true),
                    nombre = table.Column<string>(nullable: true),
                    usCeliaco = table.Column<bool>(nullable: false),
                    usLacteos = table.Column<bool>(nullable: false),
                    usTipo = table.Column<int>(nullable: false),
                    usVegano = table.Column<bool>(nullable: false),
                    usVegetariano = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_usuarios", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "usuarios");
        }
    }
}
