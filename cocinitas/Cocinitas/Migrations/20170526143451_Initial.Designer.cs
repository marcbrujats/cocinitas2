﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Cocinitas.Models;

namespace Cocinitas.Migrations
{
    [DbContext(typeof(CocinitasContext))]
    [Migration("20170526143451_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Cocinitas.Models.usuarios", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("administrador");

                    b.Property<string>("email");

                    b.Property<string>("nombre");

                    b.Property<bool>("usCeliaco");

                    b.Property<bool>("usLacteos");

                    b.Property<int>("usTipo");

                    b.Property<bool>("usVegano");

                    b.Property<bool>("usVegetariano");

                    b.HasKey("ID");

                    b.ToTable("usuarios");
                });
        }
    }
}
