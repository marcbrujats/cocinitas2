﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Cocinitas.Models;

namespace Cocinitas.Migrations
{
    [DbContext(typeof(CocinitasContext))]
    partial class CocinitasContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Cocinitas.Models.usuarios", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("administrador");

                    b.Property<string>("email");

                    b.Property<string>("nombre");

                    b.Property<bool>("usCeliaco");

                    b.Property<bool>("usLacteos");

                    b.Property<int>("usTipo");

                    b.Property<bool>("usVegano");

                    b.Property<bool>("usVegetariano");

                    b.HasKey("ID");

                    b.ToTable("usuarios");
                });
        }
    }
}
