﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Cocinitas.Models;

namespace Cocinitas.Models
{
    public class CocinitasContext : DbContext
    {
        public CocinitasContext (DbContextOptions<CocinitasContext> options)
            : base(options)
        {
        }

        public DbSet<Cocinitas.Models.usuarios> usuarios { get; set; }

        public DbSet<Cocinitas.Models.recetas> recetas { get; set; }
    }
}
