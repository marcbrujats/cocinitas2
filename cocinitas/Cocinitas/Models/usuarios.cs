﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;


namespace Cocinitas.Models
{
    public class usuarios
    {
        // prova rama usuarios
        public int ID { get; set; }
        public string usNombre { get; set; }
        public int usVegetariano { get; set; }
        public int usVegano { get; set; }
        public int usCeliaco { get; set; }
        public int usLacteos { get; set; }
        public int usTipo { get; set; }
        public string usEmail { get; set; }
        public string usPassword { get; set; }

        public int controlUsuario(string email, string password)
        {
            //este metodo devolvera un entero que indicara el resultado de la comprovacion de usuario
            int autentificacion;

            SqlConnection myConnection = dbCon.openConn();
            if (myConnection != null)
            {
                try
                {
                    //solicitamos a la BBDD la contraseña correspondiente al email introducido
                    SqlDataReader myReader = null;
                    string sqlOrder = "SELECT ID, usNombre, usVegetariano, usVegano, usCeliaco, usLacteos, usTipo, usEmail, usPassword FROM usuarios WHERE usEmail = @usEmail";
                    SqlCommand selectDatos = new SqlCommand(sqlOrder, myConnection);
                    selectDatos.Parameters.AddWithValue("@usEmail", email);
                    myReader = selectDatos.ExecuteReader();

                    myReader.Read();

                    if (myReader.HasRows) //Si el myReader contiene algun valor significa que el usuario existe
                    {
                        if (myReader["usPassword"].Equals(password))
                        {
                            autentificacion = 1; // Contraseña correcta

                            this.ID = int.Parse(myReader["ID"].ToString());
                            this.usNombre = myReader["usNombre"].ToString();
                            this.usVegetariano = ((bool)myReader["usVegetariano"]) ? 1 : 0;
                            this.usVegano = ((bool)myReader["usVegano"]) ? 1 : 0;
                            this.usCeliaco = ((bool)myReader["usCeliaco"]) ? 1 : 0;
                            this.usLacteos = ((bool)myReader["usLacteos"]) ? 1 : 0;
                            this.usTipo = int.Parse(myReader["usTipo"].ToString());
                            this.usEmail = email;
                        }
                        else
                        {
                            this.usEmail = email;
                            autentificacion = 0; // Contraseña incorrecta
                        }
                    }
                    else //myReader vacio -> el usuario no existe
                    {
                        autentificacion = -1;
                    }
                 }
                catch (Exception e)
                {
                    autentificacion = -2; //ha habido un error en la comprobacion
                }
                dbCon.closeConn();
            }
            else
            {
                autentificacion = -3; //no se ha establecido la conexion con la BBDD
            }

            return autentificacion;
        }

        public int controlRegistro(string nombre, string email)
        {
            //este metodo devolvera un entero que indicara el resultado de la comprovacion de usuario
            int autentificacion;

            SqlConnection myConnection = dbCon.openConn();
            if (myConnection != null)
            {
                try
                {
                    //comprobar en la BBDD si el usario y email ya existen 

                    SqlDataReader myReader = null;
                    string sqlOrder = "SELECT usNombre FROM usuarios WHERE usNombre = @Nombre or  usEmail = @Email";
                    SqlCommand myCmd = new SqlCommand(sqlOrder, myConnection);
                    myCmd.Parameters.AddWithValue("@Nombre", nombre);
                    myCmd.Parameters.AddWithValue("@Email", email);
                    myReader = myCmd.ExecuteReader();
                    myReader.Read();
                    
                    if ((myReader.HasRows))
                    {
                        if (myReader.HasRows)
                        {
                            autentificacion = 0; // Nombre ya existe
                        } else
                        {
                            autentificacion = 1; // Email ya existe 
                        }

                    } else {
                        autentificacion = 4;
                    }
                }
                catch (Exception e)
                {
                    autentificacion = 2; //ha habido un error en la comprovacion
                }
                dbCon.closeConn();
            }
            else
            {
                autentificacion = 3; //no se ha establecido la conexion con la BBDD
            }
            return autentificacion;
        }
    
        public int creaUsuario(string nombre, int vegetariano, int vegano, int celiaco, int lacteos, int tipo, string email, string password)
        {
            int autentificacion;
            SqlConnection myConnection = dbCon.openConn();
            if (myConnection != null)
            {
                try
                {
                    autentificacion = 0;

                    SqlDataReader myReader = null;
                    string sqlOrder = "INSERT INTO usuarios(usNombre,usVegetariano,usVegano,usCeliaco,usLacteos,usTipo,usEmail,usPassword)" +
                    " OUTPUT INSERTED.ID VALUES (@Nombre, @Vegatariano, @Vegano, @Celiaco, @Lacteos, @Tipo, @Email, @Password)";
                    SqlCommand myCmd = new SqlCommand(sqlOrder, myConnection);
                    myCmd.Parameters.AddWithValue("@Nombre", nombre);
                    myCmd.Parameters.AddWithValue("@Vegatariano", vegetariano);
                    myCmd.Parameters.AddWithValue("@Vegano", vegano);
                    myCmd.Parameters.AddWithValue("@Celiaco", celiaco);
                    myCmd.Parameters.AddWithValue("@Lacteos", lacteos);
                    myCmd.Parameters.AddWithValue("@Tipo", tipo);
                    myCmd.Parameters.AddWithValue("@Email", email);
                    myCmd.Parameters.AddWithValue("@Password", password);
                    
                    myReader = myCmd.ExecuteReader();
                }
                catch (Exception e)
                {
                    autentificacion = 1; //ha habido un error en la creación 
                }
                dbCon.closeConn();
            }
            else
            {
                autentificacion = 2; //no se ha establecido la conexion con la BBDD
            }

            return autentificacion;
        }
    }
}
