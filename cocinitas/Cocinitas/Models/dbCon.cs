﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Cocinitas.Models
{
    public class dbCon
    {
        private static SqlConnection Conn = null;

        public static void closeConn()
        {
            if (Conn != null)
            {
                try
                {
                    Conn.Close();
                    Conn = null;
                }
                catch (Exception e)
                {
                    Conn = null;
                    Console.WriteLine(e.ToString());
                }
            }
        }

        public static SqlConnection openConn()
        {
            if (Conn == null)
            {

                string connstring = "user id=sad;" +
                           "password=;server=(LocalDB)\\MSSQLLocalDB;" +
                           "Trusted_Connection=yes;" +
                           "database=Cocinitas; " +
                           "connection timeout=30";

                Conn = new SqlConnection(connstring);

                try
                {
                    Conn.Open();
                    return Conn;
                }
                catch (Exception e)
                {
                    Conn = null;
                    Console.WriteLine(e.ToString());
                    Console.ReadKey();
                    return null;
                }
            }
            else
            {
                return Conn;
            }
        }


    }
}
