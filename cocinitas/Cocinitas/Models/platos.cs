﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Data.SqlClient;


namespace Cocinitas.Models
{
    public class platos
    {

        int ivegetariano;
        int ivegano;
        int iceliaco;
        int ilacteos;
        int idesayuno;
        int ientrante;
        int isegundo;
        int ipostre;
        int isinalc;
        int ialc;
        string ingrediente1;
        string ingrediente2;
        string ingrediente3;
        string ingrediente4;
        string ingrediente5;
        string ingrediente6;
        string ingrediente7;
        string ingrediente8;
        string ingrediente9;
        string ingrediente10;
        string ingrediente11;
        string ingrediente12;
        string noingrediente1;
        string noingrediente2;
        string noingrediente3;
        string noingrediente4;
        string noingrediente5;
        string noingrediente6;



        int length;
        List<int> listplatos = new List<int>();


        public void RellenaPlato(string ingrediente1, string ingrediente2, string ingrediente3, string ingrediente4, string ingrediente5, string ingrediente6, string ingrediente7, string ingrediente8, string ingrediente9, string ingrediente10, string ingrediente11, string ingrediente12, string noingrediente1, string noingrediente2, string noingrediente3, string noingrediente4, string noingrediente5, string noingrediente6, string vegetariano, string vegano, string celiaco, string lacteo, string desayuno, string entrante, string segundo, string postre, string sinalc, string alc)
        {
            ivegetariano = (vegetariano == "on") ? 1 : 0;
            ivegano = (vegano == "on") ? 1 : 0;
            iceliaco = (celiaco == "on") ? 1 : 0;
            ilacteos = (lacteo == "on") ? 1 : 0;
            idesayuno = (desayuno == "on") ? 1 : 0;
            ientrante = (entrante == "on") ? 1 : 0;
            isegundo = (segundo == "on") ? 1 : 0;
            ipostre = (postre == "on") ? 1 : 0;
            isinalc = (sinalc == "on") ? 1 : 0;
            ialc = (alc == "on") ? 1 : 0;
            this.ingrediente1 = ingrediente1;
            this.ingrediente2 = ingrediente2;
            this.ingrediente3 = ingrediente3;
            this.ingrediente4 = ingrediente4;
            this.ingrediente5 = ingrediente5;
            this.ingrediente6 = ingrediente6;
            this.ingrediente7 = ingrediente7;
            this.ingrediente8 = ingrediente8;
            this.ingrediente9 = ingrediente9;
            this.ingrediente10 = ingrediente10;
            this.ingrediente11 = ingrediente11;
            this.ingrediente12 = ingrediente12;
            this.noingrediente1 = noingrediente1;
            this.noingrediente2 = noingrediente2;
            this.noingrediente3 = noingrediente3;
            this.noingrediente4 = noingrediente4;
            this.noingrediente5 = noingrediente5;
            this.noingrediente6 = noingrediente6;

        }

        public recetas determinaorden()
        {

            recetas mostrar = new recetas();

            if (idesayuno == 1) { mostrar = this.Buscaplato(1); }
            if (ientrante == 1) { mostrar = this.Buscaplato(2); }
            if (isegundo == 1) { mostrar = this.Buscaplato(3); }
            if (ipostre == 1) { mostrar = this.Buscaplato(4); }
            if (isinalc == 1) { mostrar = this.Buscaplato(5); }
            if (ialc == 1) { mostrar = this.Buscaplato(6); }

            return mostrar;
        }



        public recetas Buscaplato(int orden)
        {
            recetas final = new recetas();
            string error = null;
            bool platof = false;


            while (idesayuno == 1)
            {
                int[] ingredientes = new int[18];
                SqlConnection myConnection = dbCon.openConn();
                SqlDataReader myReader = null;
                for (int u = 1; u < 19; u++)
                {

                    string sqlOrder = "SELECT ID FROM ingredientes WHERE nombre = @nombre";
                    SqlCommand selectDatos = new SqlCommand(sqlOrder, myConnection);
                    switch (u)
                    {
                        case 1:
                            selectDatos.Parameters.AddWithValue("@nombre", ingrediente1);
                            break;
                        case 2:
                            selectDatos.Parameters.AddWithValue("@nombre", ingrediente2);
                            break;
                        case 3:
                            selectDatos.Parameters.AddWithValue("@nombre", ingrediente3);
                            break;
                        case 4:
                            selectDatos.Parameters.AddWithValue("@nombre", ingrediente4);
                            break;
                        case 5:
                            selectDatos.Parameters.AddWithValue("@nombre", ingrediente5);
                            break;
                        case 6:
                            selectDatos.Parameters.AddWithValue("@nombre", ingrediente6);
                            break;
                        case 7:
                            selectDatos.Parameters.AddWithValue("@nombre", ingrediente7);
                            break;
                        case 8:
                            selectDatos.Parameters.AddWithValue("@nombre", ingrediente8);
                            break;
                        case 9:
                            selectDatos.Parameters.AddWithValue("@nombre", ingrediente9);
                            break;
                        case 10:
                            selectDatos.Parameters.AddWithValue("@nombre", ingrediente10);
                            break;
                        case 11:
                            selectDatos.Parameters.AddWithValue("@nombre", ingrediente11);
                            break;
                        case 12:
                            selectDatos.Parameters.AddWithValue("@nombre", ingrediente12);
                            break;
                        case 13:
                            selectDatos.Parameters.AddWithValue("@nombre", noingrediente1);
                            break;
                        case 14:
                            selectDatos.Parameters.AddWithValue("@nombre", noingrediente2);
                            break;
                        case 15:
                            selectDatos.Parameters.AddWithValue("@nombre", noingrediente3);
                            break;
                        case 16:
                            selectDatos.Parameters.AddWithValue("@nombre", noingrediente4);
                            break;
                        case 17:
                            selectDatos.Parameters.AddWithValue("@nombre", noingrediente5);
                            break;
                        case 18:
                            selectDatos.Parameters.AddWithValue("@nombre", noingrediente6);
                            break;
                    }

                    myReader = selectDatos.ExecuteReader();
                    myReader.Read();
                    ingredientes[u] = int.Parse(myReader["ID"].ToString());
                }

                int x;
                Random rnd = new Random();
                x = rnd.Next(1, 13);


                if (myConnection != null)
                {
                    try
                    {

                        myReader.Dispose();
                        SqlDataReader myReader2 = null;
                        string sqlOrder2 = "SELECT ID_plato FROM ingredientes_platos WHERE ID_ingrediente = @iding";
                        SqlCommand selectDatos2 = new SqlCommand(sqlOrder2, myConnection);
                        selectDatos2.Parameters.AddWithValue("@iding", ingredientes[x]);
                        myReader2 = selectDatos2.ExecuteReader();
                        while (myReader2.Read())
                        {
                            myReader2.Read();
                            listplatos.Add(int.Parse(myReader2["ID_plato"].ToString()));
                            int length = listplatos.Count();
                        }

                        do
                        {
                            if (myReader2.HasRows)
                            {
                                Random rnd2 = new Random();
                                int y = rnd2.Next(0, length);
                                myReader2.Dispose();

                                SqlDataReader myReader3 = null;
                                string sqlOrder3 = "SELECT * FROM platos WHERE ID = @ID_plato AND orden= @orden";
                                SqlCommand selectDatos3 = new SqlCommand(sqlOrder3, myConnection);
                                selectDatos3.Parameters.AddWithValue("@ID_plato", listplatos[y]);
                                selectDatos3.Parameters.AddWithValue("@orden", orden);
                                myReader3 = selectDatos3.ExecuteReader();
                                myReader3.Read(); //cada Read només llegeix una fila, s'ha de posar un while perquè vagi llegint tots els valors
                                final.plNombre = myReader3["nombre"].ToString();
                                final.plVegetariano = (int)myReader3["vegetariano"];
                                final.plVegano = (int)myReader3["vegano"];
                                final.plCeliaco = (int)myReader3["celiaco"];
                                final.plLacteos = (int)myReader3["lacteos"];
                                final.plTiempo = myReader3["tiempo"].ToString();
                                final.plOrden = int.Parse(myReader3["orden"].ToString());
                                final.plCalorias = myReader3["calorias"].ToString();
                                final.plingredientes = myReader3["ingredientes"].ToString();
                                final.plreceta = myReader3["receta"].ToString();
                                final.IDus = int.Parse(myReader3["ID_usuario"].ToString());
                                myReader3.Dispose();

                                SqlDataReader myReader4 = null;
                                string sqlOrder4 = "SELECT ID_ingrediente FROM ingredientes_platos WHERE ID = @ID_plato";
                                SqlCommand selectDatos4 = new SqlCommand(sqlOrder4, myConnection);
                                selectDatos4.Parameters.AddWithValue("@ID_plato", listplatos[y]);
                                myReader4 = selectDatos4.ExecuteReader();
                                int[] ingredientesf = new int[4];
                                int z = 0;
                                while (myReader4.Read())
                                {
                                    myReader4.Read();
                                    ingredientesf[z] = int.Parse(myReader4["ID_ingrediente"].ToString());
                                    z++;
                                }


                                if (final.plVegetariano == ivegetariano && final.plVegano == ivegano && final.plCeliaco == iceliaco && final.plLacteos == ilacteos)
                                {
                                    int contplatof = 0;
                                    for (int i = 0; i < 4; i++)
                                    {
                                        for (int j = 13; j < 19; j++)
                                        {
                                            if (ingredientesf[i] == ingredientes[j])
                                            {
                                                contplatof++;
                                            }

                                        }
                                    }
                                    if (contplatof == 0)
                                    {
                                        platof = true;
                                        this.idesayuno = 2;
                                    }
                                }
                                else
                                {
                                    platof = false;
                                }
                            }


                            else
                            {
                                error = "Ninguna receta contiene " + ingredientes[x];
                                platof = true;
                            }

                        } while (platof == false);

                    }




                    catch (Exception e)
                    {

                    }



                }
            }
            return final;
        }
    }
    }


