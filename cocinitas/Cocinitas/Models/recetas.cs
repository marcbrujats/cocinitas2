﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
//using System.Data.SqlClient.
using Microsoft.AspNetCore.Http;


namespace Cocinitas.Models
{

    public class recetas
    {
        public int ID { get; set; }
        public string plNombre { get; set; }
        public int plVegetariano { get; set; }
        public int plVegano { get; set; }
        public int plCeliaco { get; set; }
        public int plLacteos { get; set; }
        public string plTiempo { get; set; }
        public int plOrden { get; set; }
        public string plCalorias { get; set; }
        public string ingrediente1 { get; set; }
        public string ingrediente2 { get; set; }
        public string ingrediente3 { get; set; }
        public string ingrediente4 { get; set; }
        public int plImg { get; set; }
        public int IDus { get; set; }
        public string plingredientes { get; set; }
        public string plreceta { get; set; }
        public int problema = 0;

        //constructores

        public recetas(string plNombre, int plVegetariano, int plVegano, int plCeliaco, int plLacteos, string plTiempo, int plOrden, string plCalorias, string ingrediente1, string ingrediente2, string ingrediente3, string ingrediente4, string ingredientestexto, string recetatexto)
        {
            this.plNombre = plNombre;
            this.plVegetariano = plVegetariano;
            this.plVegano = plVegano;
            this.plCeliaco = plCeliaco;
            this.plLacteos = plLacteos;
            this.plTiempo = plTiempo;
            this.plOrden = plOrden;
            this.plCalorias = plCalorias;
            this.ingrediente1 = ingrediente1;
            this.ingrediente2 = ingrediente2;
            this.ingrediente3 = ingrediente3;
            this.ingrediente4 = ingrediente4;
            this.plingredientes = ingredientestexto;
            this.plreceta = recetatexto;

        }

        public recetas() { }


        public string verificar()
        {
            String error = "";
            if (this.plVegano == 1 && this.plLacteos == 1)
            {
                //no puede ser lactoso y vegano
                error = "No puede ser un plato Vegano y que contenga Lactosa<br>";
            }
            if (!(Int32.TryParse(this.plTiempo, out int resultado)))
            {
                //tiempo no en numeros
                error = error + "El tiempo debe darse en numeros<br>";
            }
            if (!(Int32.TryParse(this.plCalorias, out resultado)))
            {
                //calorias no en numero
                error = error + "Las calorias deben darse en numeros<br>";
            }
            return error;
        }

        public void subereceta()
        {
            //este metodo devolvera un entero que indicara el resultado de la comprovacion de usuario

            SqlConnection myConnection = dbCon.openConn();
            if (myConnection != null)
            {


                try
                {

                    //INSERT a la BBDD


                    string sqlOrder = "INSERT INTO platos (nombre, orden, vegetariano, vegano, celiaco, lacteos, tiempo, calorias, ID_usuario, ingredientes, receta)" +
                    " OUTPUT INSERTED.ID VALUES (@plNombre, @plOrden, @plVegetariano, @plVegano, @plCeliaco, @plLacteos, @plTiempo, @plCalorias, @IDus, @plingredientes, @plreceta)";
                    SqlCommand insertplatocmd = new SqlCommand(sqlOrder, myConnection);
                    insertplatocmd.Parameters.AddWithValue("@plNombre", this.plNombre);
                    insertplatocmd.Parameters.AddWithValue("@plOrden", this.plOrden);
                    insertplatocmd.Parameters.AddWithValue("@plVegetariano", this.plVegetariano);
                    insertplatocmd.Parameters.AddWithValue("@plVegano", this.plVegano);
                    insertplatocmd.Parameters.AddWithValue("@plCeliaco", this.plCeliaco);
                    insertplatocmd.Parameters.AddWithValue("@plLacteos", this.plLacteos);
                    insertplatocmd.Parameters.AddWithValue("@plTiempo", this.plTiempo);
                    insertplatocmd.Parameters.AddWithValue("@plCalorias", this.plCalorias);
                    insertplatocmd.Parameters.AddWithValue("@plImg", this.plImg);
                    insertplatocmd.Parameters.AddWithValue("@IDus", this.IDus);
                    insertplatocmd.Parameters.AddWithValue("@plingredientes", this.plingredientes);
                    insertplatocmd.Parameters.AddWithValue("@plreceta", this.plreceta);

                    this.ID = (Int32)insertplatocmd.ExecuteScalar();

                }
                catch (Exception e)
                {
                    //falla la creación de platos
                    this.problema = 1;
                }
                dbCon.closeConn();
            }
        }





        public void ingplato()
        {
            SqlConnection myConnection = dbCon.openConn();
            if (myConnection != null)
            {


                try
                {

                    string ingrediente = "";

                    for (int i = 1; i < 5; i++)
                    {

                        switch (i)
                        {
                            case 1:
                                ingrediente = this.ingrediente1;
                                break;
                            case 2:
                                ingrediente = this.ingrediente2;
                                break;
                            case 3:
                                ingrediente = this.ingrediente3;
                                break;
                            case 4:
                                ingrediente = this.ingrediente4;
                                break;
                        }

                        int IDingrediente;

                        if (ingrediente != null)
                        {
                            //SqlConnection miCon = dbCon.openConn();
                            if (myConnection != null)
                            {
                                try // Obtenemos el ID del ingrediente de la tabla "ingredientes"
                                {


                                    SqlDataReader myReader = null;
                                    string sqlOrder = "SELECT ID FROM ingredientes WHERE nombre = '" + ingrediente + "'";
                                    SqlCommand myPavo = new SqlCommand(sqlOrder, myConnection);
                                    myReader = myPavo.ExecuteReader();
                                    myReader.Read();

                                    if (myReader.HasRows)
                                    {

                                        string pavo = myReader["ID"].ToString();
                                        IDingrediente = int.Parse(pavo);

                                        myReader.Dispose();

                                        string pavorder = "INSERT INTO ingredientes_platos (ID_ingrediente, ID_plato) VALUES (@IDingrediente, @IDplato)"; //REVISAR idPlato
                                        SqlCommand mypavocmd = new SqlCommand(pavorder, myConnection);
                                        mypavocmd.Parameters.AddWithValue("@IDingrediente", IDingrediente);
                                        mypavocmd.Parameters.AddWithValue("@IDplato", this.ID);
                                        mypavocmd.ExecuteNonQuery();


                                    }
                                    else
                                    {
                                        IDingrediente = -3; //El ingrediente no esta en la BBDD   
                                    }
                                }
                                catch (Exception e)
                                {


                                    IDingrediente = -2; //Error al buscar en la BBDD
                                }

                                //try //Relacionamos la receta con sus ingredientes en la tabla "ingredientes_platos"
                                //{
                                //    string sqlOrder = "INSERT INTO ingredientes_platos (ID_ingrediente, ID_plato) VALUES (@IDingrediente, @IDplato);"; //REVISAR idPlato
                                //    SqlCommand mypavocmd = new SqlCommand(sqlOrder, myConnection);
                                //    mypavocmd.Parameters.AddWithValue("@IDingrediente", IDingrediente);
                                //    mypavocmd.Parameters.AddWithValue("@IDplato", this.ID);

                                //}
                                //catch (Exception e)
                                //{
                                //    IDingrediente = -5; //Error al insertar en la BBDD
                                //}

                            }
                            else
                            {
                                IDingrediente = -4; //No se ha establecido la conexion con la BBDD
                            }
                        }
                        else
                        {
                            IDingrediente = -1; //Campo ingrediente vacio
                        }
                    }

                }
                catch (Exception e)
                {
                    //falla la creación ingredientes platos
                    this.problema = 2;
                }
                dbCon.closeConn();
            }

        }



        
    
}

        
    
}
