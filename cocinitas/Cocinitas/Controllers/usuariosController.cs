using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Cocinitas.Models;
using Microsoft.AspNetCore.Http;


namespace Cocinitas.Controllers
{
    public class usuariosController : Controller
    {
        private readonly CocinitasContext _context;

        public usuariosController(CocinitasContext context)
        {
            _context = context;
        }

        usuarios user = new usuarios();

        public async Task<IActionResult> login(string password, string email = null)
        {

            if (HttpContext.Session.GetString("usNombre") == null)
            {

                if (email == null)
                {
                    ViewData["missatge"] = "";
                }
                else
                {
                    switch (user.controlUsuario(email, password))
                    {
                        case 0:
                            ViewData["missatge"] = "Password incorrecto";
                            HttpContext.Session.SetString("usEmail", user.usEmail);
                            break;
                        case 1:
                            //Guardamos la informacion sobre el usuario en la sesion
                            HttpContext.Session.SetInt32("usID", user.ID);
                            HttpContext.Session.SetString("usNombre", user.usNombre);
                            HttpContext.Session.SetInt32("usVegetariano", user.usVegetariano);
                            HttpContext.Session.SetInt32("usVegano", user.usVegano);
                            HttpContext.Session.SetInt32("usCeliaco", user.usCeliaco);
                            HttpContext.Session.SetInt32("usLacteos", user.usLacteos);
                            HttpContext.Session.SetInt32("usTipo", user.usTipo);
                            HttpContext.Session.SetString("usEmail", user.usEmail);

                            return Redirect("/home");
                        case -1:
                            ViewData["missatge"] = "El email introducido no es v�lido";
                            break;
                        case -2:
                            ViewData["missatge"] = "Se ha producido un error, intente ingresar de nuevo";
                            break;
                        case -3:
                            ViewData["missatge"] = "No se ha podido establecer la conexi�n, vuelva a intentar m�s tarde";
                            break;
                    }
                }
                return View();
            }
            else
            {
                return Redirect("/home");
            }
        }

        public IActionResult logout()
        {
            HttpContext.Session.Clear();
            return Redirect("/home");
        }

        public async Task<IActionResult> registro(string email, string password, string ivegetariano, string ivegano, string iceliaco, string ilacteos, string nombre = null)
        {
            if (HttpContext.Session.GetString("usNombre") == null)
            {

                int vegetariano = 0;
                int vegano = 0;
                int celiaco = 0;
                int lacteos = 0;

                if (ivegetariano == "on")
                {
                    vegetariano = 1;
                }

                if (ivegano == "on")
                {
                    vegano = 1;
                }

                if (iceliaco == "on")
                {
                    celiaco = 1;
                }

                if (ilacteos == "on")
                {
                    lacteos = 1;
                }

                bool registrar = false;
                usuarios user = new usuarios();
                if (nombre !=null)
                {
                    switch (user.controlRegistro(nombre, email))
                    {
                        case 0:
                            ViewData["missatge"] = "Error: El nombre de usuario ya existe";
                            break;
                        case 1:
                            ViewData["missatge"] = "Error: La direcci�n de correo electr�nico ya existe";
                            break;
                        case 2:
                            ViewData["missatge"] = "Se ha producido un error, intente ingresar de nuevo";
                            break;
                        case 3:
                            ViewData["missatge"] = "No se ha podido establecer la conexi�n, vuelva a intentar m�s tarde";
                            break;
                        case 4:
                            registrar = true;
                            break;
                    }

                    if (registrar == true)
                    {
                        int tipo = 5;
                        switch (user.creaUsuario(nombre, vegetariano, vegano, celiaco, lacteos, tipo, email, password))
                        {
                            case 0:
                                ViewData["missatge"] = "Alta realizada correctamente";
                                break;
                            case 1:
                                ViewData["missatge"] = "Se ha producido un error, intente ingresar de nuevo";
                                break;
                            case 2:
                                ViewData["missatge"] = "No se ha podido establecer la conexi�n, vuelva a intentar m�s tarde";
                                break;
                        }
                    }
                }

                return View();
            }
            else
            {
                return Redirect("/home");
            }
        }
  
    }
}
