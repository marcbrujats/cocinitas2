using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Cocinitas.Models;
using Microsoft.AspNetCore.Http;

namespace Cocinitas.Controllers
{
    public class recetasController : Controller
    {
        private readonly CocinitasContext _context;

        int vegeta = 0;
        int vegan = 0;
        int celia = 0;
        int lacte = 0;

        public recetasController(CocinitasContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> subereceta(string nombre, string vegetariano, 
        string vegano, string celiaco, string lacteos, string tiempo, int orden, 
        string calorias, string ingrediente1, string ingrediente2, string ingrediente3, 
        string ingrediente4, string ingredientestexto, string recetatexto)  
        {

            ViewData["usNombre"] = HttpContext.Session.GetString("usNombre");
            
            if (nombre == null || tiempo == null || calorias == null)
            {

                ViewData["errores"] = "Rellene los campos con *";
                return View();
            }
            else
            {
                


                compruebacheckbox(vegetariano, vegano, celiaco, lacteos);




                recetas receta = new recetas(nombre, vegeta, vegan, celia, lacte, tiempo, orden, calorias, ingrediente1, ingrediente2, ingrediente3, ingrediente4, ingredientestexto, recetatexto);

                //obtengo la id del usuario de la sesion actual
                
                receta.IDus = int.Parse(HttpContext.Session.GetInt32("usID").ToString());



                string error = receta.verificar();

                if (error.Equals(""))
                {
                    receta.subereceta();
                    receta.ingplato();
                }
                else
                {
                    //error de formulario
                    receta.problema = 3;

                }

                switch (receta.problema)
                {
                    case 1:
                        ViewData["errores"] = "tabla platos no recibe";
                        break;
                    case 2:
                        ViewData["errores"] = "tabla ingplatos no recibe";
                        break;
                    case 3:
                        ViewData["errores"] = error;
                        break;

                }

                ViewData["idplato"] = receta.ID;
                return View();
            }

            //return View();
            
            
        }

        public void compruebacheckbox(string vegetariano, string vegano, string celiaco, string lacteos)
        {
            if (vegetariano=="on")
            {
                vegeta = 1;
            }
            
            if (vegano=="on")
            {
                vegan = 1;
            }
            if (celiaco== "on")
            {
                celia = 1;
            }
            if (lacteos== "on")
            {
                lacte = 1;
            }
        }

        




    }
}