﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Cocinitas.Models;
using Microsoft.AspNetCore.Http;

namespace Cocinitas.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewData["usNombre"] = HttpContext.Session.GetString("usNombre");
            return View();
        }

        public IActionResult Presentacion()
        {
            return View();
        }

        public IActionResult muestrareceta()
        {
            return View();
        }

        public IActionResult MuestraPlato(string ingrediente1, string ingrediente2, string ingrediente3, string ingrediente4, string ingrediente5, string ingrediente6, string ingrediente7, string ingrediente8, string ingrediente9, string ingrediente10, string ingrediente11, string ingrediente12, string noingrediente1, string noingrediente2, string noingrediente3, string noingrediente4, string noingrediente5, string noingrediente6, string vegetariano, string vegano, string celiaco, string lacteo, string desayuno, string entrante, string segundo, string postre, string sinalc, string alc)
        {

            

            platos buscarplato = new platos();
            buscarplato.RellenaPlato(ingrediente1, ingrediente2, ingrediente3, ingrediente4, ingrediente5, ingrediente6, ingrediente7, ingrediente8, ingrediente9, ingrediente10, ingrediente11, ingrediente12, noingrediente1, noingrediente2, noingrediente3, noingrediente4, noingrediente5, noingrediente6, vegetariano, vegano, celiaco, lacteo, desayuno, entrante, segundo, postre, sinalc, alc);
            recetas mostrar = new recetas(); 
            mostrar = buscarplato.determinaorden();

            ViewData["plNombre"] = mostrar.plNombre;
            ViewData["plTiempo"] = mostrar.plTiempo;
            ViewData["plCalorias"] = mostrar.plCalorias;
            ViewData["plVegano"] = mostrar.plVegano;
            ViewData["plVegetariano"] = mostrar.plVegetariano;
            ViewData["plCeliaco"] = mostrar.plCeliaco;
            ViewData["plLacteos"] = mostrar.plLacteos;
            ViewData["plingredientes"] = mostrar.plingredientes;
            ViewData["plreceta"] = mostrar.plreceta;
            ViewData["plOrden"] = mostrar.plOrden;
            ViewData["plImg"] = mostrar.plImg;

            return Redirect("/home"); // Redirigir a la página de mostrar recetas o dónde sea para mostrar el plato que nos ha encontrado el método anterior
        }
        
        public IActionResult Error()
        {
            return View();
        }

        
    }
}
